import CartParser from "./CartParser";
import * as uuid from "uuid";
import * as path from "path";
import cartJSON from "../samples/cart.json";

let parser, parseLine, validate, parse;

beforeEach(() => {
  parser = new CartParser();
  parseLine = parser.parseLine.bind(parser);
  validate = parser.validate.bind(parser);
  parse = parser.parse.bind(parser);
});

describe("CartParser - unit tests", () => {
  describe("parseLine", () => {
    it("should return format object when given string", () => {
      const fakeId = "mockId";

      uuid.v4 = jest.fn(() => fakeId);

      expect(parseLine("Mollis consequat,9.00,2")).toEqual({
        id: fakeId,
        name: "Mollis consequat",
        price: 9,
        quantity: 2,
      });
    });
  });
  describe("validate", () => {
    it("should return empty array when given correct content", () => {
      const content = `Product name,Price,Quantity
		  Mollis consequat,9.00,2
		  Tvoluptatem,10.32,1`;

      expect(validate(content)).toEqual([]);
    });

    // it("should return array with error when given empty string", () => {
    //   const content = ``;

    //   expect(validate(content)).toEqual([
    //     {
    //       column: 0,
    //       message: "Expected content but received empty file.",
    //       row: 0,
    //       type: "empty",
    //     },
    //   ]);
    // });

    it("should return array witn header type error when given content with wrong header", () => {
      const content = `Product anme,Price,Quantity
		  Mollis consequat,9.00,2
		  Tvoluptatem,10.32,1`;

      expect(validate(content)).toEqual([
        {
          column: 0,
          message:
            'Expected header to be named "Product name" but received Product anme.',
          row: 0,
          type: "header",
        },
      ]);
    });

    it("should return array witn row type error when given content with wrong row", () => {
      const content = `Product name,Price,Quantity
			Mollis consequat,2.00,9
			Tvoluptatem`;

      expect(validate(content)).toEqual([
        {
          column: -1,
          message: "Expected row to have 3 cells but received 1.",
          row: 2,
          type: "row",
        },
      ]);
    });

    it("should return array witn cell type error when given content with empty cell", () => {
      const content = `Product name,Price,Quantity
			  ,2.00,9
			  Tvoluptatem,10.32,1`;

      expect(validate(content)).toEqual([
        {
          column: 0,
          message: 'Expected cell to be a nonempty string but received "".',
          row: 1,
          type: "cell",
        },
      ]);
    });

    it("should return array witn cell type error when given content with negative cell value", () => {
      const content = `Product name,Price,Quantity
		  Mollis consequat,-2.00,9
				  Tvoluptatem,10.32,1`;

      expect(validate(content)).toEqual([
        {
          column: 1,
          message:
            'Expected cell to be a positive number but received "-2.00".',
          row: 1,
          type: "cell",
        },
      ]);
    });

    it("should return array witn cell type error when given content with NaN cell value", () => {
      const content = `Product name,Price,Quantity
		  Mollis consequat,NaN,9
				  Tvoluptatem,10.32,1`;

      expect(validate(content)).toEqual([
        {
          column: 1,
          message: 'Expected cell to be a positive number but received "NaN".',
          row: 1,
          type: "cell",
        },
      ]);
    });

    it("should return array witn all types errors when given content with all errors", () => {
      const content = `Product nmee,Price,Quantity
		  ,-2.00,9
				  Tvoluptatem,10.32`;

      expect(validate(content)).toEqual([
        {
          column: 0,
          message:
            'Expected header to be named "Product name" but received Product nmee.',
          row: 0,
          type: "header",
        },
        {
          column: 0,
          message: 'Expected cell to be a nonempty string but received "".',
          row: 1,
          type: "cell",
        },
        {
          column: 1,
          message:
            'Expected cell to be a positive number but received "-2.00".',
          row: 1,
          type: "cell",
        },
        {
          column: -1,
          message: "Expected row to have 3 cells but received 2.",
          row: 2,
          type: "row",
        },
      ]);
    });
  });
  describe("parse", () => {
    it("should throw error when validation failed", () => {
      console.error = jest.fn();
      parser.readFile = jest.fn(() => {
        return `roduct name,Price,Quantity
		Mollis consequat,9.00,2
		Tvoluptatem,10.32,1`;
      });
      expect(() => {
        parse("mockPath");
      }).toThrow(Error("Validation failed!"));
    });
  });
});

describe("CartParser - integration test", () => {
  describe("parse", () => {
    it("should correct parse file", () => {
      const cartCsvPath = path.join(
        path.dirname(__dirname),
        "samples",
        "cart.csv"
      );
      const mockId = "mockId";
      const mockItems = cartJSON.items.map((item) => {
        return { ...item, id: mockId };
      });
      const mockCartJSON = { ...cartJSON, items: mockItems };

      uuid.v4 = jest.fn(() => mockId);
      expect(parse(cartCsvPath)).toEqual(mockCartJSON);
    });
  });
});
